<?php

namespace App\Controller\Api\v1;

use App\Entity\Result;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use App\Service\GithubService;
use App\Repository\ResultRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class WordController extends AbstractController {

    /**
     * @Route("/api/v1/score/{term}")
     */
    public function getTermPopularity($term, GithubService $service, ResultRepository $repository) {
        // If there's a cached result return it from the database
        if($result = $repository->findByTerm($term)) {
            return $this->json($result[0], 200, ['Content-Type' => 'application/json']);
        }

        $score = $service->getTermScore($term);

        // Persist the results
        $repository->insert($term, $score);

        $result = new Result();
        $result->setTerm($term);
        $result->setScore($score);
        
        return $this->json($result, 200, ['Content-Type' => 'application/json']);
    }

}
