<?php

namespace App\Controller\Api\v2;

use App\Entity\Result;
use App\Service\GithubService;
use App\Repository\ResultRepository;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class TermPopularityController extends AbstractController {

    const MEDIA_TYPE = 'application/vnd.api+json';

    private $httpClient;

    private $response = [
        'links' => [
            'self' => ''
        ],
        'meta' => [
            'copyright' => 'Copyright 2013 - 2020 Undabot',
            'authors' => [
                'Marino Bjelopera'
            ],
        ],
        'data' => [],
    ];

    private $headers = [
        'Content-Type' => self::MEDIA_TYPE,
    ];


    public function __construct() {
        $this->httpClient = HttpClient::create([
            'headers' => $this->headers
        ]);
    }


    /**
     * @Route("/api/v2/score/{term}")
     */
    public function getTermPopularity($term, Request $request, GithubService $service, ResultRepository $repository) {
        
        if($headerError = $this->checkHeaders($request)) {
            return $this->json($headerError, $headerError['errors']['status'], $this->headers);
        }

        $this->response['links']['self'] = $request->getUri();

        // If there's a cached result return it from the database
        if($result = $repository->findByTerm($term)) {
            $result = $result[0];
            $response = $this->fillResponseData('results', $result->getId(), $result->getTerm(), $result->getScore());
            return $this->json($response, 200, ['Content-Type' => self::MEDIA_TYPE]);
        }

        $score = $service->getTermScore($term);

        // persist the result
        $resultId = $repository->insert($term, $score);

        $response = $this->fillResponseData('results', $resultId, $term, $score);

        return $this->json($response, 200, ['Content-Type' => self::MEDIA_TYPE]);
    }


    /**
     * Checks request headers for JSON:API specification
     * https://jsonapi.org/format/#content-negotiation
     */
    private function checkHeaders($request): ?array {
        $errorResponse = null;

        // check Accept header if in request
        if($request->headers->has('Accept')) {
            // accept header must be JSON:API media type
            if($request->headers->get('Accept') !== self::MEDIA_TYPE) {
                $errorResponse = [
                    'errors' => [
                        'status' => 406,
                        'title' => 'Unacceptable Media Type',
                        'detail' => 'Invalid Accept header received in the request.',
                    ],
                ];
            }
        }

        // check Content-Type is present has valid media type
        if(!$request->headers->has('Content-Type') 
        || ($request->headers->get('Content-Type') !== self::MEDIA_TYPE)) {
            $errorResponse = [
                'errors' => [
                    'status' => 415,
                    'title' => 'Unsupported Media Type',
                    'detail' => 'Content-Type must be application/vnd.api+json',
                ],
            ];
        }

        return $errorResponse;
    }

    /**
     * Fills the response format with object data
     * @param string Type
     * @param int Id
     * @param string Term
     * @param float Score
     */
    private function fillResponseData($type, $id, $term, $score) {
        $this->response['data']['type'] = $type;
        $this->response['data']['id'] = $id;
        $this->response['data']['attributes']['term'] = $term;
        $this->response['data']['attributes']['score'] = $score;

        return $this->response;
    }
}
