<?php

namespace App\Repository;

use App\Entity\Result;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class ResultRepository extends ServiceEntityRepository {


    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Result::class);
    }


    /**
     * Persist a Result object into the DB
     * @param string Requested term
     * @param float Score for the given term
     */
    public function insert($term, $score) {
        $entityManager = $this->getEntityManager();

        $result = new Result();
        $result->setTerm($term);
        $result->setScore($score);

        $entityManager->persist($result);
        $entityManager->flush();

        return $result->getId();
    }


    /**
     * Retrieves a Result object from the database if it exists
     * @param string Requested term
     * @return array Array representing Result object or null
     */
    public function findByTerm($term): ?array {

        $query = $this->createQueryBuilder('r')
                ->where('r.term = :term')
                ->setParameter('term', $term)
                ->setMaxResults(1);

        $query = $query->getQuery();
        $result = $query->execute();

        if($result) {
            return $result;
        }

        return null;
    }

}
