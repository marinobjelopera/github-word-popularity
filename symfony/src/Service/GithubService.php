<?php

namespace App\Service;

use App\Entity\Result;
use Symfony\Component\HttpClient\HttpClient;


class GithubService {

    private $baseUrl = 'https://api.github.com/search/';
    private $resultCount = 0;
    private $httpClient;


    public function __construct() {
        // Instantiate HttpClient with necessary headers
        $this->httpClient = HttpClient::create([
            'headers' => [
                'Content-Type' => 'application/json'
            ]
        ]);
    }


    /**
     * Retrieves positive results for the given term
     * @param string Requested term
     * @return int number of positive results
     */
    private function getTermPositiveResults($term): int {
        $response = $this->httpClient->request('GET', "{$this->baseUrl}issues?q={$term}+rocks in:title is:issue");
        $array = $response->toArray();

        $this->resultCount = $array['total_count'];

        return $array['total_count'];
    }


    /**
     * Retrieves negative results for the given term
     * Adds the results to total count
     * @param string Requested term
     */
    private function getTermNegativeResults($term) {
        $response = $this->httpClient->request('GET', "{$this->baseUrl}issues?q={$term}+sucks in:title is:issue");
        $array = $response->toArray();

        $this->resultCount += $array['total_count'];
    }


    /**
     * Calculates the score for the given term
     * @param string Requested term
     * @return float Score for the term
     */
    public function getTermScore($term) {
        $positiveResults = $this->getTermPositiveResults($term);
        $this->getTermNegativeResults($term);

        $totalResults = $this->resultCount;
        // reset the resultCount
        $this->resultCount = 0;

        if($totalResults < 1 || $positiveResults < 1) {
            return 0;
        }

        return ($totalResults / $positiveResults);
    }
}
