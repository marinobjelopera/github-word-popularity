<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ResultRepository")
 */
class Result {

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $term;

    /**
     * @ORM\Column(type="float", scale=2)
     */
    private $score;


    // Getters

    public function getId() {
        return $this->id;
    }

    public function getTerm() {
        return $this->term;
    }

    public function getScore() {
        return $this->score;
    }

    // Setters

    public function setTerm($term) {
        $this->term = $term;
    }

    public function setScore($score) {
        $this->score = $score;
    }
}
