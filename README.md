# **Github Word Popularity Search**

### GitHub Term Popularity is a Restful api for retrieving score from 0 - 10 for the given term within GitHub issues, fetched by GitHub v3 api. The api is written in Symfony 5 and uses Docker.

&nbsp;

# Getting Started

To start, download or clone the repository, then follow these steps:

1. Open your terminal or bash
2. Run `docker-compose up -d --build` to build and run the necessary Docker volumes
3. If you change the user and/or password of the MySQL database be sure to update Docker compose yaml file, as well as `.env` DATABASE_URL
4. You can now send requests to localhost:8001, see Examples

&nbsp;

# Examples

## API v1

**GET** &nbsp;&nbsp; /api/v1/score/linux &nbsp;&nbsp;**HTTP/1.1**

```javascript
{
    "id": 1,
    "term": "linux",
    "score": 7.5
}
```

&nbsp;

## API v2 - JSON:API specification

**GET** &nbsp;&nbsp; /api/v2/score/linux &nbsp;&nbsp;**HTTP/1.1**

```javascript
{
    "links": {
        "self": "http://host:port/api/v2/score/linux"
    },
    "meta": {
        "copyright": "Copyright 2013 - 2020 Undabot",
        "authors": [
            "Marino Bjelopera"
        ]
    },
    "data": {
        "type": "results",
        "id": 1,
        "attributes": {
            "term": "linux",
            "score": 7.5
        }
    }
}
```

&nbsp;

# What's Next
In order to upgrade the project, simply replicate the steps from Getting Started. To add additional endpoints, create a Controller which consumes a Service. If the controller needs to communicate with the Database, make sure to create Entities that represent the tables and EntityRepository which will be used to send queries to the database itself. Inject Services and/or Repositories into Controllers to reduce the complexity of the Controllers and to move the business logic outside of the Controllers.
